import { defineConfig } from 'vite'

// https://vitejs.dev/config/
export default defineConfig({
  // https://vitejs.dev/guide/static-deploy.html#gitlab-pages-and-gitlab-ci
  // https://aitorjs.gitlab.io/vite-ts-age-calculator-app
  base: '/vite-ts-age-calculator-app',
  test: {
    environment: 'jsdom'
  }
})