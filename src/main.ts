import { getAge } from './logic';
import './style.css';

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
<main>
    <div class="container">
      <header>
        <form name="valform" action="" method="POST">
          <div>
            <span>Day</span>
            <span>
              <input type="text" placeholder="DD" id="day" maxlength="2" />
            </span>
            <span id="error-day"></span>
          </div>

          <div>
            <span>Month</span>
            <span>
              <input type="text" placeholder="MM" id="month" maxlength="2" />
            </span>
            <span id="error-month"></span>
          </div>

          <div>
            <span>Year</span>
            <span>
              <input type="text" placeholder="YYYY" id="year" maxlength="4" />
            </span>
            <span id="error-year"></span>
          </div>
        </form>
      </header>

      <div class="line">
        <img src="./assets/images/icon-arrow.svg" alt="alt" style="pointer-events: none;opacity:0.5" />
      </div>

      <div class="results">
        <p>
          <span id="result-year">--</span>
          <span>years</span>
        </p>

        <p>
          <span id="result-month">--</span>
          <span>months</span>
        </p>

        <p>
          <span id="result-day">--</span>
          <span>days</span>
        </p>
      </div>

    </div>
  </main>`


let validation = {
  day: false,
  month: false,
  year: false
}
const months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

const day_ = document.getElementById("day") as HTMLInputElement;
const month_ = document.getElementById("month") as HTMLInputElement;
const year_ = document.getElementById("year") as HTMLInputElement;
const img_ = document.querySelector(".line img") as HTMLInputElement;

day_.addEventListener('keydown', (_event) => validDay());
month_.addEventListener('keydown', (_event) => validMonth());
year_.addEventListener('keydown', (_event) => validYear());

day_.addEventListener('input', (_event) => day_.value = onlyNumbers(day_.value));
month_.addEventListener('input', (_event) => month_.value = onlyNumbers(month_.value));
year_.addEventListener('input', (_event) => year_.value = onlyNumbers(year_.value));

img_.addEventListener('click', (_event) => handleClick());

function validDay() {
  setTimeout(() => {
    const day = Number((document.getElementById("day") as HTMLInputElement).value);
    const month = Number((document.getElementById("month") as HTMLInputElement).value);
    const errorDay = document.getElementById("error-day") as HTMLInputElement;
    errorDay.innerText = "";

    if (day === 0 || !day || (day < 1 || day > 31) || (month && day > months[month - 1])) {
      errorOnDay();
      return;
    }

    validation.day = true;
    deleteErrorDay();

    isFormValid();
  }, 100)
}

function validMonth() {
  setTimeout(() => {
    const day = Number((document.getElementById("day") as HTMLInputElement).value);
    const month = Number((document.getElementById("month") as HTMLInputElement).value);
    const errorMonth = document.getElementById("error-month") as HTMLInputElement;
    errorMonth.innerText = "";

    if (month === 0 || !month || month < 1 || month > 12) {
      errorOnMonth();
      return;
    }

    if (day > months[month - 1]) {
      (document.getElementById("error-day") as HTMLInputElement).innerText = "Not valid day";
      validation.day = false;
      displayErrorDay();

      validation.month = true;
      deleteErrorMonth();

      isFormValid();
      return;
    }

    validation.month = true;
    deleteErrorMonth();

    isFormValid();
  }, 100)
}

function validYear() {
  setTimeout(() => {
    const year = Number((document.getElementById("year") as HTMLInputElement).value);
    const now = new Date();
    const ourYear = now.getFullYear();

    const errorYear = document.getElementById("error-year") as HTMLInputElement;
    errorYear.innerText = "";

    if (year > ourYear || year < 1000 || !year) {
      errorOnYear();
      return;
    }

    validation.year = true;
    deleteErrorYear();

    isFormValid();
  }, 100)
}

function handleClick() {
  const year = (document.getElementById("year") as HTMLInputElement).value;
  const month = (document.getElementById("month") as HTMLInputElement).value;
  const day = (document.getElementById("day") as HTMLInputElement).value;
  const data = `${year}-${month}-${day}`;

  const ageResult = getAge(data);
  const dayAll = document.getElementById("result-day") as HTMLInputElement;
  dayAll.style.animationName = "pulse";
  dayAll.style.animationDuration = "1s";
  dayAll.style.animationFillMode = "both";

  (document.getElementById("result-year") as HTMLInputElement).innerText = String(ageResult.year);
  (document.getElementById("result-month") as HTMLInputElement).innerText = String(ageResult.month);
  (document.getElementById("result-day") as HTMLInputElement).innerText = String(ageResult.day);
}



function isFormValid() {
  const img = document.querySelector(".line img") as HTMLInputElement;
  if (validation.day && validation.month && validation.year) {
    img.style.pointerEvents = "auto";
    img.style.opacity = "1";
    img.style.cursor = "pointer";
    return
  }
  img.style.pointerEvents = "none";
  img.style.opacity = "0.5";
  img.style.cursor = "none";
}

function displayErrorDay() {
  (document.getElementById("day") as HTMLInputElement).style.border = "1px solid red";
  (document.getElementById("day") as HTMLInputElement).style.boxShadow = "0px 0px 10px red";
}

function deleteErrorDay() {
  (document.getElementById("day") as HTMLInputElement).style.border = "2px solid #dbdbdb";
  (document.getElementById("day") as HTMLInputElement).style.boxShadow = "none";
}

function errorOnDay() {
  (document.getElementById("error-day") as HTMLInputElement).innerText = "Not valid day";
  displayErrorDay();
  validation.day = false;
  isFormValid();
}

function displayErrorMonth() {
  (document.getElementById("month") as HTMLInputElement).style.border = "1px solid red";
  (document.getElementById("month") as HTMLInputElement).style.boxShadow = "0px 0px 10px red";
}

function deleteErrorMonth() {
  (document.getElementById("month") as HTMLInputElement).style.border = "2px solid #dbdbdb";
  (document.getElementById("month") as HTMLInputElement).style.boxShadow = "none";
}

function errorOnMonth() {
  (document.getElementById("error-month") as HTMLInputElement).innerText = "Not valid month";
  displayErrorMonth();
  validation.month = false;
  isFormValid();
}

function displayErrorYear() {
  (document.getElementById("year") as HTMLInputElement).style.border = "1px solid red";
  (document.getElementById("year") as HTMLInputElement).style.boxShadow = "0px 0px 10px red";
}

function deleteErrorYear() {
  (document.getElementById("year") as HTMLInputElement).style.border = "2px solid #dbdbdb";
  (document.getElementById("year") as HTMLInputElement).style.boxShadow = "none";
}

function errorOnYear() {
  (document.getElementById("error-year") as HTMLInputElement).innerText = "Not valid year";
  displayErrorYear();
  validation.year = false;
  isFormValid();
}

function onlyNumbers(value: string) {
  return value
    .replace(/[^0-9.]/g, '')
    .replace(/(\..*?)\..*/g, '$1');
}

// setupCounter(document.querySelector<HTMLButtonElement>('#counter')!)
//setupMain(document.getElementById('day')!)
