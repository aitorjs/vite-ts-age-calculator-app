export function getAge(data: string, now?: any) {
  if (!now) now = new Date(new Date().setHours(0, 0, 0, 0));

  const selectedData: any = new Date(new Date(data).setHours(0, 0, 0, 0));
  const diff = new Date(now - selectedData);

  return {
    "year": diff.getFullYear() - 1970,
    "month": diff.getMonth(),
    "day": diff.getDate() - 1
  }
}