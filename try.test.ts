import { describe, expect, it } from 'vitest';
import { getAge } from './src/logic';

// query utilities:
import {
  getByLabelText,
  getByText,
  getByTestId,
  queryByTestId,
  // Tip: all queries are also exposed on an object
  // called "queries" which you could import here as well
  waitFor,
  screen,
  fireEvent,
  findByText,
  queryByText,
} from '@testing-library/dom'
// import fireEvent from "@testing-library/user-event"
// adds special assertions like toHaveTextContent
// import '@testing-library/jest-dom'


function getExampleDOM() {
  const dom = `<main><div class="container">
      <header>
        <form name="valform" action="" method="POST">
          <div>
            <span>Day</span>
            <span>
              <input type="text" placeholder="DD" id="day" data-testid="day" maxlength="2" />
            </span>
            <span id="error-day" data-testid="error-day"></span>
          </div>

          <div>
            <span>Month</span>
            <span>
              <input type="text" placeholder="MM" id="month" maxlength="2" />
            </span>
            <span id="error-month"></span>
          </div>

          <div>
            <span>Year</span>
            <span>
              <input type="text" placeholder="YYYY" id="year" maxlength="4" />
            </span>
            <span id="error-year"></span>
          </div>
        </form>
      </header>

      <div class="line">
        <img src="./assets/images/icon-arrow.svg" alt="alt" style="pointer-events: none;opacity:0.5" />
      </div>

      <div class="results">
        <p>
          <span id="result-year">--</span>
          <span>years</span>
        </p>

        <p>
          <span id="result-month">--</span>
          <span>months</span>
        </p>

        <p>
          <span id="result-day">--</span>
          <span>days</span>
        </p>
      </div>

    </div>
    </main>
    
    <script>
    console.log('hola')
    </script>`;

  const div = document.createElement('div')
  div.innerHTML = dom;
  return div;
}

describe('testing library DOM', () => {
  it('Should get correct age', () => {
    const container = getExampleDOM()
    const day = getByTestId(container, 'day');

    expect(day.getAttribute('value')).toBeFalsy()

    const a = fireEvent.keyDown(day, { key: 'A', code: 'KeyA' })
    const day2 = getByText(container, 'Not valid day');
    console.log("PASA", a, day2)

    // (document.getElementById("day") as HTMLInputElement).value = "0";
    // expect(errorDay).toBeTruthy();
  })
})

describe('age calculator', () => {
  it('Should get correct age', () => {
    const now = new Date('2023-08-31').setHours(0, 15, 0, 0);
    const selectData = '1111-1-1';

    const age = getAge(selectData, now);
    expect(age).toStrictEqual({ year: 912, month: 7, day: 30 });
  })

  it('Should get correct age', () => {
    const now = new Date('2023-08-31').setHours(0, 15, 0, 0);
    const selectData = '1111-1-1';

    const age = getAge(selectData, now);
    expect(age).toStrictEqual({ year: 912, month: 7, day: 30 });
  })

  // on frontend, only numbers are allowed
  it('Should get NaN', () => {
    const now = new Date('2023-08-31').setHours(0, 15, 0, 0);
    const selectData = "1111-1-1m";

    const age = getAge(selectData, now);
    expect(age).toStrictEqual({ year: NaN, month: NaN, day: NaN });
  })
})